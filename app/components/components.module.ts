import {NgModule} from "@angular/core";
import {TarefaComponent} from "./tarefa/tarefa.component";
import {NativeScriptCommonModule} from "nativescript-angular/common";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    declarations: [
        TarefaComponent
    ],
    exports: [
        TarefaComponent
    ]
})
export class ComponentsModule {

}