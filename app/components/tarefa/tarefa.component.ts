import {Component, Input} from "@angular/core";

@Component({
    selector: 'Tarefa',
    moduleId: module.id,
    templateUrl: 'tarefa.component.html',
    styleUrls: ['tarefa.component.scss']
})
export class TarefaComponent {

    @Input() item: number;

}