"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TarefaComponent = (function () {
    function TarefaComponent() {
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], TarefaComponent.prototype, "item", void 0);
    TarefaComponent = __decorate([
        core_1.Component({
            selector: 'Tarefa',
            moduleId: module.id,
            templateUrl: 'tarefa.component.html',
            styleUrls: ['tarefa.component.scss']
        })
    ], TarefaComponent);
    return TarefaComponent;
}());
exports.TarefaComponent = TarefaComponent;
