"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var entidade_1 = require("./entidade");
var Tarefa = (function (_super) {
    __extends(Tarefa, _super);
    function Tarefa() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Tarefa.prototype, "titulo", {
        get: function () {
            return this._titulo;
        },
        set: function (value) {
            this._titulo = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tarefa.prototype, "observacao", {
        get: function () {
            return this._observacao;
        },
        set: function (value) {
            this._observacao = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tarefa.prototype, "dia", {
        get: function () {
            return this._dia;
        },
        set: function (value) {
            this._dia = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tarefa.prototype, "horaInicio", {
        get: function () {
            return this._horaInicio;
        },
        set: function (value) {
            this._horaInicio = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tarefa.prototype, "horaFim", {
        get: function () {
            return this._horaFim;
        },
        set: function (value) {
            this._horaFim = value;
        },
        enumerable: true,
        configurable: true
    });
    return Tarefa;
}(entidade_1.Entidade));
exports.Tarefa = Tarefa;
