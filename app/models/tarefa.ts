import {Entidade} from "./entidade";

export class Tarefa extends Entidade {

    private _titulo: string;
    private _observacao?: string;
    private _dia: number;
    private _horaInicio: Date;
    private _horaFim: Date;

    get titulo(): string {
        return this._titulo;
    }

    set titulo(value: string) {
        this._titulo = value;
    }

    get observacao(): string {
        return this._observacao;
    }

    set observacao(value: string) {
        this._observacao = value;
    }

    get dia(): number {
        return this._dia;
    }

    set dia(value: number) {
        this._dia = value;
    }

    get horaInicio(): Date {
        return this._horaInicio;
    }

    set horaInicio(value: Date) {
        this._horaInicio = value;
    }

    get horaFim(): Date {
        return this._horaFim;
    }

    set horaFim(value: Date) {
        this._horaFim = value;
    }
}