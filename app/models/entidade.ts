export class Entidade {

    private _id_usuario: number;


    get id_usuario(): number {
        return this._id_usuario;
    }

    set id_usuario(value: number) {
        this._id_usuario = value;
    }
}