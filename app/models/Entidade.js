"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Entidade = (function () {
    function Entidade() {
    }
    Object.defineProperty(Entidade.prototype, "id_usuario", {
        get: function () {
            return this._id_usuario;
        },
        set: function (value) {
            this._id_usuario = value;
        },
        enumerable: true,
        configurable: true
    });
    return Entidade;
}());
exports.Entidade = Entidade;
