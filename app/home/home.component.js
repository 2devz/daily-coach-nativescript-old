"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var enums_1 = require("tns-core-modules/ui/enums");
var HomeComponent = (function () {
    function HomeComponent() {
        this.sideOpen = false;
        this.items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
        console.log('teste');
        this.sideElement = this.sider.nativeElement;
        console.log(this.sideElement);
    };
    HomeComponent.prototype.tap = function () {
        console.log(this.sideElement);
        this.sideAnimation();
    };
    HomeComponent.prototype.sideAnimation = function () {
        this.sideOpen = !this.sideOpen;
        this.sideElement.animate({
            translate: { x: this.sideOpen ? 80 : 0, y: 0 },
            duration: 300,
            curve: enums_1.AnimationCurve.easeInOut
        });
    };
    __decorate([
        core_1.ViewChild('sider'),
        __metadata("design:type", core_1.ElementRef)
    ], HomeComponent.prototype, "sider", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['home.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
