import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {StackLayout} from "tns-core-modules/ui/layouts/stack-layout";
import {AnimationCurve} from "tns-core-modules/ui/enums";
import {NavigationButton} from "tns-core-modules/ui/action-bar";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

    items: Array<any>;

    sideOpen: boolean = false;

    @ViewChild('sider') sider: ElementRef;

    sideElement: StackLayout;

    constructor() {
        this.items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
        console.log('teste');
        this.sideElement = this.sider.nativeElement;
        console.log(this.sideElement);
    }

    tap() {
        console.log(this.sideElement);
        this.sideAnimation();
    }

    sideAnimation() {
        this.sideOpen = !this.sideOpen;
        this.sideElement.animate({
            translate: {x: this.sideOpen ? 80 : 0, y: 0},
            duration: 300,
            curve: AnimationCurve.easeInOut
        })
    }
}
